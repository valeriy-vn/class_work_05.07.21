/* За каждую операцию будет отвечать отдельная функция, т.е. для сложения - add(a,b), для умножения - multiple(a,b) и т.д.
 Каждая из них принимает в аргументы только два числа и возвращает результат операции над двумя числами Если число не передано
  в функцию аргументом - ПО УМОЛЧАНИЮ присваивать этому аргументу 0.


Основная функция calculate()
Принимает ТРИ АРГУМЕНТА:
1 - число
2 - число
3 - функция которую нужно выполнить для двух этих чисел. Таким образом получается что основная функция калькулятор
 будет вызывать переданную ей аргументом функцию для двух чисел, которые передаются остальными двумя аргументами.
  При делении на 0 выводить ошибку. Функия калькулятор доджна принять на вход 3 аругмента, Если аргументов больше
   или меньше выводить ошибку.
*/
function add (a=0,b=0) {
    return a + b;
};

function sub (a=0,b=0) {
    return a - b;
};

function multiple (a=0, b=0) {
    return a * b;
};

function div (a=0, b=0) {
    return b = 0 ? "Ошибка. Делить на ноль нельзя": a / b;

};

function calculate (x,y,callback){
    if (arguments.length !== 3) {
        console.error("Вывели недопустимое количество аргументов")
        return;
    };
    return callback(x,y);

}

function show (){
    const r = parseInt(prompt("Введите первое число"));
    const k = parseInt(prompt("Введите второе число")); 
    const sign=prompt("Введите знак арифметической операции")

    switch (sign){
        case '+': console.log(calculate(r, k, add));
        break;
        case '-': console.log(calculate(r, k, sub));
        break;
        case '*': console.log(calculate(r, k, multiple));
        break;
        case '/': console.log(calculate(r, k, div));
        break;
    }

}
show();